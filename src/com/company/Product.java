package com.company;

public class Product implements Comparable<Product> {

    private String name;
    private Integer price;
    private String size;

    public Product(String name, Integer price, String size) {
        this.name = name;
        this.price = price;
        this.size = size;
    }

    @Override
    public int compareTo(Product o) {
        return name.compareTo(o.getName());
    }

    public Integer getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}
