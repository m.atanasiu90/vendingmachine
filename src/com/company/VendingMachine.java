package com.company;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class VendingMachine {
    private Map<String, Tray> stock;
    private InputOutputService inputOutputService;

    public VendingMachine() {
        this.stock = new TreeMap<>();
        this.inputOutputService = new InputOutputService();
        for (int index = 0; index < 10; index++) {
          /*  String prefix = "00"; //solutia 1
            if (index >= 9) {
                prefix = "0";
            }*/
            String prefix = index >= 9 ? "0" : "00"; // solutia 2
            stock.put(prefix + (index + 1), new Tray());
        }
    }

    public void start() {
        do {
            inputOutputService.displayMainMenu(stock);
            String userInput = inputOutputService.getUserInput();
            process(userInput);
        } while (productsAvailable());
    }

    private boolean productsAvailable() {
        for (Tray tray : stock.values()) {
            if (tray.isNotEmpty()) {
                return true;
            }
        }
        return false;
    }

    private void process(String userInput) {

        boolean validationSuccessful = isValidat(userInput);
        if (!validationSuccessful) {
            inputOutputService.displayErrorForInput(userInput)
            return;
        }

    }

    private boolean isValidat(String userInput) {
        Set<String> productCodes = stock.keySet();
        boolean codeExist = productCodes.contains(userInput);
        if (!codeExist) {
            return false;
        }
        Tray tray = stock.get(userInput);
        return tray.isNotEmpty();

    }


    public void loadProducts() {
        int index = 0;
        for (Map.Entry<String, Tray> entry : stock.entrySet()) {

            Tray tray = entry.getValue();
            tray.add(ProductFactory.buildProduct(ProductType.get(index++), 10));


        }


    }
}
