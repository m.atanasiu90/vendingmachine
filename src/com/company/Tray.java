package com.company;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

public class Tray {
    private Queue<Product> products;

    public Tray(){
        this.products = new PriorityQueue<>();
    }

    public void add(Product... productsArray) {
        products.addAll(Arrays.asList(productsArray));


    }
    public Product peek(){
        return products.peek();
    }


    public boolean isNotEmpty() {
        return !products.isEmpty();
    }
}
