package com.company;

public class ProductFactory {
    public static Product[] buildProduct(ProductType productType, Integer numberOfProducts) {
        Product[] result = new Product[numberOfProducts];
        for (int index = 0; index < numberOfProducts; index++) {
            Product product = new Product(productType.getName(), productType.getPrice(), productType.getSize());
            result[index] = product;



        }

        return result;
    }

}
